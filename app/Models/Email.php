<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;

    protected $table = "email";

    protected $fillable = [
        'employee_id',
        'email',
        'is_primary'
    ];

    public function employee(){
        return $this->belongsTo(Employee::class, 'id');
    }

    public function getIsCheckedAttribute(){
        if($this->is_primary == 1){
            return 'checked';
        }else{
            return '';
        }
    }
}
