<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    
    protected $table = "employee_details";

    protected $fillable = [
        'employee_name',
        'address1',
        'address2',
        'location',
        'postal_code',
        'postal_area',
        'taluka',
        'suburb',
        'east_west',
        'city',
        'district',
        'state',
        'country'
    ];

    public function emails(){
        return $this->hasMany(Email::class);
    }

    public function contacts(){
        return $this->hasMany(Contact::class);
    }

    public function whatsappContacts(){
        return $this->hasMany(Whatsapp::class);
    }

    
}
