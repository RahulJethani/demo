<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Email;
use App\Models\Employee;
use App\Models\Whatsapp;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        return view('employee.index', compact([
            'employees'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $employee = Employee::create([
            'employee_name' => $request->employee_name,
            'east_west' => $request->east_west,
            'location' => $request->location,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'postal_code' => $request->postal_code,
            'postal_area' => $request->postal_area,
            'taluka' => $request->taluka,
            'suburb' => $request->suburb,
            'city' => $request->city,
            'district' => $request->district,
            'state' => $request->state,
            'country' => $request->country
        ]);

        
        foreach($request->whatsapp_number as $whatsapp){
            $employee->whatsappContacts()->create([
                'whatsapp_number'=> $whatsapp,
            ]);

        }

        $whatsapp_contact = Whatsapp::where('whatsapp_number', $request->is_whatsapp_primary)->first();
        
        $whatsapp_contact->is_primary = 1;
        $whatsapp_contact->save();
            
        


        foreach($request->contact as $cont){
            $employee->contacts()->create([
                'contact'=> $cont
            ]);
        }

        $normal_contact = Contact::where('contact', $request->is_contact_primary)->first();
        $normal_contact->is_primary = 1;
        $normal_contact->save();

        foreach($request->email as $mail){
            $employee->emails()->create([
                'email'=> $mail
            ]);
        }

        $mail = Email::where('email', $request->is_email_primary)->first();
        $mail->is_primary = 1;
        $mail->save();

        return redirect(route('employee.index'));


        
    }

    private function getContactPrimaryStatus(Request $request, $number){
        if($number == $request->is_primary_contact){
            return 1;
        } else{
            return 0;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $emp = Employee::where('id', $employee->id)->first();
        $contacts = Contact::all()->where('employee_id', $employee->id);
        $whatsapp_contacts = Whatsapp::all()->where('employee_id', $employee->id);
        $emails = Email::all()->where('employee_id', $employee->id);

        return view('employee.edit', compact([
            'emp',
            'contacts',
            'whatsapp_contacts',
            'emails'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->update([
           'employee_name' => $request->employee_name,
           'east_west' => $request->east_west,
           'location' => $request->location,
           'address1' => $request->address1,
           'address2' => $request->address2,
           'postal_code' => $request->postal_code,
           'postal_area' => $request->postal_area,
           'taluka' => $request->taluka,
           'suburb' => $request->suburb,
           'city' => $request->city,
           'district' => $request->district,
           'state' => $request->state,
           'country' => $request->country 
        ]);

        $contacts = Contact::all()->where('employee_id', $employee->id);
        foreach($contacts as $contact){
            $contact->delete();
        }

        $whatsapp_contacts = Whatsapp::all()->where('employee_id', $employee->id);
        foreach($whatsapp_contacts as $contact){
            $contact->delete();
        }

        $emails = Email::all()->where('employee_id', $employee->id);
        foreach($emails as $email){
            $email->delete();
        }

        foreach($request->whatsapp_number as $whatsapp){
            $employee->whatsappContacts()->create([
                'whatsapp_number'=> $whatsapp,
            ]);

        }

        $whatsapp_contact = Whatsapp::where('whatsapp_number', $request->is_whatsapp_primary)->first();
        
        $whatsapp_contact->is_primary = 1;
        $whatsapp_contact->save();
            
        


        foreach($request->contact as $cont){
            $employee->contacts()->create([
                'contact'=> $cont
            ]);
        }

        $normal_contact = Contact::where('contact', $request->is_contact_primary)->first();
        $normal_contact->is_primary = 1;
        $normal_contact->save();

        foreach($request->email as $mail){
            $employee->emails()->create([
                'email'=> $mail
            ]);
        }

        $mail = Email::where('email', $request->is_email_primary)->first();
        $mail->is_primary = 1;
        $mail->save();

        return redirect(route('employee.index'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
