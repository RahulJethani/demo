<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  
    <div class="container">
      <div class="card">
      <div class="card-header">Edit Employee</div>
      <div class="card-body">
        <form action="{{ route('employee.update', $emp->id) }}" method="POST" id="myForm">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="employee_name">Employee Name:</label>
          <input type="text" name="employee_name" id="employee_name" value="{{ $emp->employee_name }}">
        </div>
        <h4>Address</h4>
        <div class="address-block">

        <div class="row">
        <div class="col-4">
        <label for="address">Address 1:</label>
        <input type="text" placeholder="address 1" name="address1" id="address1" value="{{ ($emp->address1) }}">
        </div>
        <div class="col-4">
        <label for="address">Address 2:</label>
        <input type="text" placeholder="address2" name="address2" id="address2" value="{{ $emp->address2 }}">
        </div>
        <div class="col-4">
        <label for="address">Location:</label>
        <input type="text" placeholder="location" name="location" id="location" value="{{ $emp->location }}">
        </div>
        </div>
        

        <div class="row">
        <div class="col-4">
        <label for="address">Postal area:</label>
        <input type="text" placeholder="postal_area" name="postal_area" id="postal_area" value="{{ $emp->postal_area }}">
        </div>
        <div class="col-4">
        <label for="address">postal code:</label>
        <input type="number" placeholder="postal_code" name="postal_code" id="postal_code" value="{{ $emp->postal_code }}">
        </div>
        <div class="col-4">
        <label for="address">Taluka:</label>
        <input type="text" placeholder="taluka" name="taluka" id="taluka" value="{{ $emp->taluka }}">
        </div>
        </div>
        

        <div class="row">
        <div class="col-4">
        <label for="address">Suburb:</label>
        <input type="text" placeholder="suburb" name="suburb" id="suburb" value="{{ $emp->suburb }}">
        </div>
        <div class="col-4">
        <label for="east_west">east/west:</label>
        <input type="text" placeholder="east/west" name="east_west" id="east_west" value="{{ $emp->east_west }}">
        </div>
        <div class="col-4">
        <label for="address">City:</label>
        <input type="text" placeholder="city" name="city" id="city" value="{{ $emp->city }}">
        </div>
        </div>
        

        <div class="row">
        <div class="col-4">
        <label for="address">District:</label>
        <input type="text" placeholder="district" name="district" id="district" value="{{ $emp->district }}">
        </div>
        <div class="col-4">
        <label for="address">State:</label>
        <input type="text" placeholder="state" name="state" id="state" value="{{ $emp->state }}">
        </div>
        <div class="col-4">
        <label for="address">Country:</label>
        <input type="text" placeholder="country" name="country" id="country" value="{{ $emp->country }}">
        </div>
        </div>

        </div>
        

        <h4>Contact Details:</h4>
        <div class="row">
        <div class="col-md-4" id="contact-adder">
        <div class="heading d-flex justify-content-between">
        <h6>Contacts</h6>
        <button class="btn btn-outline-primary" class="contact-add" onclick="contactAdd()" type="button">+</button>
        </div>
        @foreach($contacts as $contact)
        <div id='contact-field' class='row'><button type='button' class='contact-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='contacts'>Contact:</label><input type='number' name='contact[]' id='contact' value="{{ $contact->contact}}"><label for='is_primary'>primary: </label><input type='radio' name='is_contact_primary'  {{ $contact->is_checked }}></div>
        @endforeach
        </div>
        <div class="col-md-4" id="whatsapp-contact-adder">
        <div class="heading d-flex justify-content-between">
        <h6>Whatsapp Contacts</h6>
        <button class="btn btn-outline-primary" class="whatsapp-contact-add" onclick="whatsappContactAdd()" type="button">+</button>
        </div>

        @foreach($whatsapp_contacts as $whatsapp)
        <div id='contact-field' class='row'><button type='button' class='contact-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='contacts'>Whatsapp:</label><input type='number' name='whatsapp_number[]' id='whatsapp_number' value="{{ $whatsapp->whatsapp_number}}"><label for='is_primary'>primary: </label><input type='radio' name='is_whatsapp_primary'  {{ $whatsapp->is_checked }}></div>
        @endforeach
        
        </div>
        
        <div class="col-md-4" id="email-adder">
        <div class="heading d-flex justify-content-between">
        <h6>Email</h6>
        <button class="btn btn-outline-primary" class="email-add" onclick="emailAdd()" type="button">+</button>
        </div>

        @foreach($emails as $mail)
        <div id='contact-field' class='row'><button type='button' class='contact-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='contacts'>Email:</label><input type='email' name='email[]' id='email' value="{{ $mail->email }}"><label for='is_primary'>primary: </label><input type='radio' name='is_email_primary'  {{ $mail->is_checked }}></div>
        @endforeach
        

        </div>
        </div>

        <div class="card-footer">
        <button type="submit" class="btn btn-primary" id="btn-submit">Save</button>
        </div>
        </form>
        </div>
      </div>
      </div>

      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script>

function contactAdd(){
      $("#contact-adder").append("<div id='contact-field' class='row'><button type='button' class='contact-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='contacts'>Contact:</label><input type='number' name='contact[]' id='contact'><label for='is_primary'>primary: </label><input type='radio' name='is_contact_primary'></div>");
    }

    function whatsappContactAdd(){
      $("#whatsapp-contact-adder").append("<div id='whatsapp-contact-field' class='row'><button type='button' class='whatsapp-contact-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='whatsapp_number'>Whatsapp:</label><input type='number' name='whatsapp_number[]' id='whatsapp_number'><label for='is_primary'>primary: </label><input type='radio' name='is_whatsapp_primary' id='whatsapp_primary'></div>");

    }

    function emailAdd(){
      $("#email-adder").append("<div id='email-field' class='row'><button type='button' class='email-delete' onclick='valueDelete(this)'><i class='fa fa-trash'></i></button><label for='email'>Email:</label><input type='email' name='email[]' id='email'><label for='is_primary'>primary: </label><input type='radio' name='is_email_primary'></div>");

    }

    function valueDelete(value){
      console.log($(value).parent());
      $(value).parent().remove();
    }

    $('#myForm').submit(function(){
      
      $("input[name='is_contact_primary']").val($("input[name='is_contact_primary']:checked").prev().prev().val());
      $("input[name='is_whatsapp_primary']").val($("input[name='is_whatsapp_primary']:checked").prev().prev().val());
      $("input[name='is_email_primary']").val($("input[name='is_email_primary']:checked").prev().prev().val());
    });
    

    </script>
  </body>
</html>